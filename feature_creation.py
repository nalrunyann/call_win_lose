import pandas as pd

FEATURES = ['user_id', 'token_id', 'start',
            'user_rating_at_call', 'calls_before_call', 'overlapping_calls',
            'average_rating_during_call', 'start_price', 'win', 'day_of_week', 'part_of_day']


class WinLossFeatures:
    def __init__(self, call_data, call_result_data, ranking_history_data):
        self.call_data = call_data
        self.call_result_data = call_result_data
        self.ranking_history_data = ranking_history_data

    def merge_data(self):
        # Merge call and call_result data
        self.merged_data = pd.merge(self.call_data, self.call_result_data, left_on='id', right_on='call_id', how='inner')
        # Convert datetime fields
        self.merged_data['start'] = pd.to_datetime(self.merged_data['start'])
        self.merged_data['finish'] = pd.to_datetime(self.merged_data['finish'])
        self.ranking_history_data['datetime'] = pd.to_datetime(self.ranking_history_data['datetime'])

    def add_user_rating_at_call(self):
        # Function to find the latest rating before each call's start time
        def get_latest_rating_before_call(row):
            user_rankings = self.ranking_history_data[
                (self.ranking_history_data['user_id'] == row['user_id']) &
                (self.ranking_history_data['datetime'] <= row['start'])]
            if not user_rankings.empty:
                latest_rating = user_rankings.loc[user_rankings['datetime'].idxmax()]
                return pd.Series([latest_rating['score'], latest_rating['calls']])
            return pd.Series([None, None])

        # Apply the function to each row in merged_call_data
        self.merged_data[['user_rating_at_call', 'calls_before_call']] = self.merged_data.apply(get_latest_rating_before_call, axis=1)

    def add_time_features(self):
        # Add duration in hours
        self.merged_data['duration_hours'] = (self.merged_data['finish'] - self.merged_data['start']).dt.total_seconds() / 3600
        # Day of the week and part of the day
        self.merged_data['day_of_week'] = self.merged_data['start'].dt.dayofweek
        self.merged_data['part_of_day'] = self.merged_data['start'].dt.hour.apply(self.part_of_day)

    def part_of_day(self, hour):
        if 5 <= hour < 12:
            return 'Morning'
        elif 12 <= hour < 17:
            return 'Afternoon'
        elif 17 <= hour < 21:
            return 'Evening'
        else:
            return 'Night'

    def add_overlapping_calls(self):
        # Function to count overlapping calls
        def count_overlapping_calls(row):
            overlapping_calls = self.merged_data[
                (self.merged_data['user_id'] == row['user_id']) &
                (self.merged_data['start'] < row['finish']) &
                (self.merged_data['finish'] > row['start'])
            ]
            return len(overlapping_calls) - 1  # Subtract 1 to exclude the current call itself

        self.merged_data['overlapping_calls'] = self.merged_data.apply(count_overlapping_calls, axis=1)

    def add_average_rating_during_call(self):
        # Function to calculate average rating during the call
        def calculate_average_rating_during_call(row):
            overlapping_calls = self.merged_data[
                (self.merged_data['start'] < row['finish']) &
                (self.merged_data['finish'] > row['start'])
            ]
            if not overlapping_calls.empty:
                return overlapping_calls['user_rating_at_call'].mean()
            return 0

        self.merged_data['average_rating_during_call'] = self.merged_data.apply(calculate_average_rating_during_call, axis=1)

    def process(self):
        self.merge_data()
        self.add_user_rating_at_call()
        self.add_time_features()
        self.add_overlapping_calls()
        self.add_average_rating_during_call()
        return self.merged_data
    
if __name__ == "__main__":
    call_data = pd.read_csv('data/call.csv')
    call_result_data = pd.read_csv('data/call_result.csv')
    ranking_history_data = pd.read_csv('data/ranking_history.csv')
    merged_data = WinLossFeatures(call_data, call_result_data, ranking_history_data).process()
    merged_data = merged_data[FEATURES]
    
    merged_data.to_csv('data/final.csv', index=False)