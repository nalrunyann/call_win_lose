import pandas as pd
from feature_creation import WinLossFeatures, FEATURES
from train import WinLossTrainer

if __name__ == "__main__":
    call_data = pd.read_csv('data/call.csv')
    call_result_data = pd.read_csv('data/call_result.csv')
    ranking_history_data = pd.read_csv('data/ranking_history.csv')
    
    merged_data = WinLossFeatures(call_data, call_result_data, ranking_history_data).process()
    merged_data = merged_data[FEATURES]
    
    trainer = WinLossTrainer(merged_data)
    trainer.run()

    # save model
    trainer.model.save_model('data/catboost_model.cbm')
    
    
    
