import catboost
import numpy as np

class Predictor:
    def __init__(self, entry):
        self.entry = entry
        self.model = catboost.CatBoostClassifier(cat_features=[7])
        self.model.load_model('model/catboost_model.cbm')
        
    def predict(self):
        probs = self.model.predict_proba(np.array([self.entry]))
        
        return {"win_chance" : probs[0][1],
                "loss_chance" : probs[0][0]}