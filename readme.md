# Call Win Lose Prediction

## Introduction
The Call Outcome prediction service

## Input Variables
Input variables can be found in /router/router.py Pydantic Base Model for entry

### src
Contains all the source code and Jupyter notebooks for model predictor component

### predictor.py
A Python script for managing prediction entry and output

## Setup Instructions

1. **Clone the repository**:
2. **Install dependencies**:
    Ensure you have Python installed, and then run:
    ```bash
    pip install -r requirements.txt
    ```
3.  **Running with Docker**
    Ensure Docker is installed and running on your system. Then execute:

    ```bash
    docker build . -t call_win_lose
    ```



