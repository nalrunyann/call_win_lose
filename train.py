import pandas as pd
from sklearn.model_selection import train_test_split
from catboost import CatBoostClassifier
from sklearn.metrics import roc_auc_score, precision_score, recall_score, f1_score, confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns

class WinLossTrainer:
    def __init__(self, data):
        self.data = data
        self.model = None
        self.X_train = None
        self.X_test = None
        self.y_train = None
        self.y_test = None

    def prepare_data(self):
        X = self.data.drop(columns=['user_id', 'start', 'win'])
        y = self.data['win']
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(
            X, y, test_size=0.2, random_state=42, stratify=y)

    def train_model(self):
        cat_features = [i for i, col in enumerate(self.X_train.columns) if self.X_train.dtypes[i] == 'object']
        self.model = CatBoostClassifier(
            iterations=10000,
            learning_rate=0.1,
            depth=4,
            loss_function='Logloss',
            verbose=1000,
            cat_features=cat_features,
            use_best_model=True,
            early_stopping_rounds=1000,
            class_weights=[1, 4]
        )
        self.model.fit(self.X_train, self.y_train, eval_set=(self.X_test, self.y_test))

    def evaluate_model(self):
        feature_importances = self.model.get_feature_importance(prettified=True)
        
        # Plot feature importances
        plt.figure(figsize=(10, 6))
        sns.barplot(x='Importances', y='Feature Id', data=feature_importances)
        plt.title('Feature importances')
        plt.show()
        
        y_pred = self.model.predict(self.X_test)
        
        conf_matrix = confusion_matrix(self.y_test, y_pred)
        sns.heatmap(conf_matrix, annot=True, fmt='d')
        plt.xlabel('Predicted classes')
        plt.ylabel('True classes')
        plt.title('Confusion matrix')
        plt.show()

    def run(self):
        self.prepare_data()
        self.train_model()
        self.evaluate_model()
        
if __name__ == "__main__":
    df = pd.read_csv('data/final.csv')
    
    trainer = WinLossTrainer(df)
    trainer.run()
    
    # save model
    trainer.model.save_model('model/catboost_model.cbm')
