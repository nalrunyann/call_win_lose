from fastapi import APIRouter
from pydantic import BaseModel, field_validator
from src.components.predictor import Predictor

class CallWinLoss(BaseModel):
    token_id: int
    user_rating_at_call: float
    calls_before_call: int
    overlapping_calls: int
    average_rating_during_call: float
    start_price: float
    day_of_week: int
    part_of_day: str
    
    # @field_validator('part_of_day')
    # def validate_part_of_day(cls, v):
    #     valid_parts = ['Night', 'Morning', 'Afternoon', 'Evening']
    #     if v not in valid_parts:
    #         raise ValueError(f"part_of_day must be one of {valid_parts}")
    #     return v
    
    
router = APIRouter()

@router.post("/win_loss")
async def win_loss(call_win_loss: CallWinLoss):
    predictor = Predictor([
        call_win_loss.token_id,
        call_win_loss.user_rating_at_call,
        call_win_loss.calls_before_call,
        call_win_loss.overlapping_calls,
        call_win_loss.average_rating_during_call,
        call_win_loss.start_price,
        call_win_loss.day_of_week,
        call_win_loss.part_of_day
    ])
    return predictor.predict()